package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.testng.AssertJUnit.assertEquals;

public class RegistrationPage extends BasePage{

    public RegistrationPage(WebDriver driver) {
        super(driver);
    }

    By singInButton = By.xpath("//a[@title='Log in to your customer account']");
    By locEmail = By.id("email_create");
    By createAccountButton = By.xpath("//i[@class='icon-user left']");
    By titleMr = By.xpath("//input[@id='id_gender1']");
    By locFirstName = By.id("customer_firstname");
    By locLastName = By.id("customer_lastname");
    By locCreatePass = By.id("passwd");
    By locBirthDayButton = By.id("days");
    By locDay = By.xpath("//select[@id=\"days\"]/option[24]");
    By locBirthMonthButton = By.id("months");
    By locMonth = By.xpath("//select[@id=\"months\"]/option[9]");
    By locBirthYearButton = By.id("years");
    By locYear = By.xpath("//select[@id=\"years\"]/option[38]");
    By locAddress = By.id("address1");
    By locCity = By.id("city");
    By locState = By.id("uniform-id_state");
    By findState = By.xpath("//select[@id=\"id_state\"]/option[15]");
    By locZipCode = By.id("postcode");
    By locCountry = By.id("id_country");
    By findCountry = By.xpath("//select[@id='id_country']/option[2]");
    By locMobile = By.id("phone_mobile");
    By locAssignAddress = By.id("alias");
    By registerButton = By.id("submitAccount");
    By locSignOut = By.xpath("//a[@title='Log me out']");

    public void registerAccount(String email, String firstName, String lastName, String pass, String address,
                                String city, String zipCode, String mobileNumber, String assignAddress) {
        driver.findElement(singInButton).click();
        waitForElementToAppear(locEmail);
        driver.findElement(locEmail).sendKeys(email);
        driver.findElement(createAccountButton).click();
        waitForElementToAppear(titleMr);
        driver.findElement(titleMr).click();
        driver.findElement(locFirstName).sendKeys(firstName);
        driver.findElement(locLastName).sendKeys(lastName);
        driver.findElement(locCreatePass).sendKeys(pass);
        driver.findElement(locBirthDayButton).click();
        driver.findElement(locDay).click();
        driver.findElement(locBirthMonthButton).click();
        driver.findElement(locMonth).click();
        driver.findElement(locBirthYearButton).click();
        driver.findElement(locYear).click();
        driver.findElement(locAddress).sendKeys(address);
        driver.findElement(locCity).sendKeys(city);
        driver.findElement(locState).click();
        driver.findElement(findState).click();
        driver.findElement(locZipCode).sendKeys(zipCode);
        driver.findElement(locCountry).click();
        driver.findElement(findCountry).click();
        driver.findElement(locMobile).sendKeys(mobileNumber);
        driver.findElement(locAssignAddress).sendKeys(assignAddress);
        driver.findElement(registerButton).click();
        assertEquals(driver.findElement(locSignOut).getText(), "Sign out");
    }

}
