package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class VerifyProductsMethodsPage extends BasePage{

    public VerifyProductsMethodsPage(WebDriver driver) {
        super(driver);
    }

    public void verifyProducts(String locElement, String forWhich) {
        List<WebElement> activeElement = driver.findElements(By.cssSelector(locElement));
        int size = activeElement.size();
        System.out.println("Number to active - " + forWhich + ":" + size);
    }
}
