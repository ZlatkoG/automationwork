package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.testng.Assert.assertEquals;

public class CartPage extends BasePage {

    public CartPage(WebDriver driver) {
        super(driver);
    }

    By continueShopping = By.xpath("//span[@class=\"continue btn btn-default button exclusive-medium\"]");
    By productFirstElement = By.xpath("//*[@id=\"homefeatured\"]/li[1]/div");
    By addToCartElementFirst = By.xpath("//*[@id=\"homefeatured\"]/li[1]/div/div[2]/div[2]/a[1]");
    By productSecondElement = By.xpath("//*[@id=\"homefeatured\"]/li[3]/div");
    By addToCartElementSecond = By.xpath("//*[@id=\"homefeatured\"]/li[3]/div/div[2]/div[2]/a[1]/span");
    By productThirdElement = By.xpath("//*[@id=\"homefeatured\"]/li[6]/div");
    By addToCartElementThird = By.xpath("//*[@id=\"homefeatured\"]/li[6]/div/div[2]/div[2]/a[1]/span");
    By goIntoCart = By.xpath("//a[@title='View my shopping cart']");
    By proceedButtonFirst = By.xpath("//a[@class=\"button btn btn-default standard-checkout button-medium\"]");
    By emailAddress = By.id("email");
    By locPassword = By.id("passwd");
    By singInButton = By.id("SubmitLogin");
    By accountName = By.cssSelector("a[title='View my customer account']");
    By proceedButtonSecond = By.xpath("//button[@class=\"button btn btn-default button-medium\"]");
    By checkboxAgree = By.id("cgv");
    By proceedButtonThird = By.xpath("//button[@class=\"button btn btn-default standard-checkout button-medium\"]");
    By productOne = By.xpath("//a[text()='Faded Short Sleeve T-shirts']");
    By productTwo = By.xpath("//a[text()='Printed Dress']");
    By productThree = By.xpath("//a[text()='Printed Summer Dress']");

    private final String email = "georgievski.zlatko@hotmail.com";
    private final String pass = "Planina99";

    private void buyProduct(By moveToElement, By addToCartElement, By elementToClickOn) {
        Actions action = new Actions(driver);
        waitForElementToAppear(moveToElement);
        action.moveToElement(driver.findElement(moveToElement))
                .moveToElement(driver.findElement(addToCartElement))
                .click().build().perform();
        waitForElementToAppear(elementToClickOn);
        driver.findElement(elementToClickOn).click();
    }

    public void buyFirstProduct() {
        buyProduct(productFirstElement, addToCartElementFirst, continueShopping);
    }

    public void buySecondProduct() {
        buyProduct(productSecondElement, addToCartElementSecond, continueShopping);
    }

    public void buyThirdProduct() {
        buyProduct(productThirdElement, addToCartElementThird, continueShopping);
    }


    public void checkCartProducts() {
        driver.findElement(goIntoCart).click();
        waitForElementToAppear(proceedButtonFirst);
        driver.findElement(proceedButtonFirst).click();
        try {
            driver.findElement(accountName);
        } catch (Exception e) {
            driver.findElement(emailAddress).sendKeys(email);
            driver.findElement(locPassword).sendKeys(pass);
            driver.findElement(singInButton).click();
        }
        driver.findElement(proceedButtonSecond).click();
        driver.findElement(checkboxAgree).click();
        driver.findElement(proceedButtonThird).click();
        driver.findElement(productOne);
        driver.findElement(productTwo);
        driver.findElement(productThree);
    }
}
