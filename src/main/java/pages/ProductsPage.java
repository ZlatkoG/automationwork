package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class ProductsPage extends BasePage{

    public ProductsPage(WebDriver driver) {
        super(driver);
    }

    By locSearch = By.id("search_query_top");
    By locButton = By.xpath("//button[@name='submit_search']");
    By locForElements = By.cssSelector("h5[itemprop='name'] a[class='product-name']");

    public void search(String text) {
        driver.findElement(locSearch).sendKeys(text);
        driver.findElement(locButton).click();
    }

    public void saveProductNamesToFile() throws IOException {
        String fileName = "output.txt";
        FileWriter writer = new FileWriter(fileName);
        List<WebElement> items = driver.findElements(locForElements);
        for (WebElement item : items) {
            writer.write(item.getText() + "\n");
        }
        writer.close();
    }

}
