package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;

import java.io.File;
import java.io.IOException;
public class Screenshot {

    private static Logger logger = LogManager.getLogger(Screenshot.class);

    private WebDriver driver;

    public static void createScreenshotsDirectory() {
        String path = System.getProperty("user.dir") + "/screenshots";
        File file = new File(path);
        try {
            file.mkdir();
            logger.info("Created screenshots directory.");
        } catch (Exception e) {
            logger.info("Directory screenshots exists.");
        }
    }

    public static void takeScreenShot(WebDriver driver, String screenShotName) {
        TakesScreenshot screenshot = (TakesScreenshot)driver;
        File source = screenshot.getScreenshotAs(OutputType.FILE);
        try {
            FileHandler.copy(source, new File("screenshots/" + screenShotName + ".png"));
        } catch (IOException e) {
            logger.error("Error while taking screenshot.");
        }
    }

}
