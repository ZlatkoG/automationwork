package drivers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Driver {

    private static Logger logger = LogManager.getLogger(Driver.class);

    public static WebDriver getDriver() {
        System.setProperty("webdriver.chrome.driver", "src\\main\\java\\drivers\\chromedriver.exe");
        logger.info("Running tests with Chrome");
        ChromeOptions options = new ChromeOptions();
        if ("true".equals(System.getProperty("headless"))) {
            logger.info("Running tests with Chrome headless");
            options = new ChromeOptions();
            options.addArguments("--headless", "window-size=1024,768", "--no-sandbox");
        }
        return new ChromeDriver(options);
    }
}
