package tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import pages.CartPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class VerifyCartProductsTest extends BaseTest {

    private static Logger logger = LogManager.getLogger(VerifyCartProductsTest.class);

    private WebDriver driver;
    private CartPage cartPage;

    @BeforeClass
    public void setUp() {
        driver = initializeDriver();
        cartPage = new CartPage(initializeDriver());
        logger.info("Go to landing page");
        driver.manage().window().maximize();
        driver.get(url);
    }

    @Test
    public void checkProducts() {
       cartPage.buyFirstProduct();
       cartPage.buySecondProduct();
       cartPage.buyThirdProduct();
       cartPage.checkCartProducts();
    }

}

