package tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import pages.RegistrationPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.StringUtils;

public class CreateAccountTest extends BaseTest {

    private static Logger logger = LogManager.getLogger(CreateAccountTest.class);

    private WebDriver driver;
    private RegistrationPage registrationPage;

    private String email = "georgievski.z" + StringUtils.randomString(6) + "@hotmail.com";
    private String firstName = "Zlatko";
    private String lastName = "Petrevski";
    private String pass = "Planina99";
    private String address = "Prvomajska";
    private String city = "Chicago";
    private String zipCode = "60007";
    private String mobileNumber = "312-509-6995";
    private String assignAddress = "Leninova";

    @BeforeClass
    public void setUp() {
        driver = initializeDriver();
        registrationPage = new RegistrationPage(initializeDriver());
        logger.info("Go to landing page");
        driver.manage().window().maximize();
        driver.get(url);
    }

    @Test
    public void createAccount() {
        registrationPage.registerAccount(email, firstName, lastName, pass, address, city, zipCode, mobileNumber, assignAddress);
    }

}
