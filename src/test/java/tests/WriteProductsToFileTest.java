package tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import pages.ProductsPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class WriteProductsToFileTest extends BaseTest {

    private static Logger logger = LogManager.getLogger(WriteProductsToFileTest.class);

    private WebDriver driver;
    private ProductsPage productsPage;

    private String text = "Printed Dresses";

    @BeforeClass
    public void setUp() {
        driver = initializeDriver();
        productsPage = new ProductsPage(initializeDriver());
        logger.info("Go to landing page");
        driver.manage().window().maximize();
        driver.get(url);
    }

    @Test
    public void printedDress() throws IOException {
        productsPage.search(text);
        productsPage.saveProductNamesToFile();
    }

}
