package tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import pages.VerifyProductsMethodsPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class VerifyPopularSellersTest extends BaseTest {

    private static Logger logger = LogManager.getLogger(VerifyPopularSellersTest.class);

    private WebDriver driver;
    private VerifyProductsMethodsPage verifyProductsMethodsPage;

    String locPopular = "ul[id='homefeatured'] li[class*='ajax_block_product']";
    String locBestSellers ="ul[id='blockbestsellers'] li[class*='ajax_block_product']";

    @BeforeClass
    public void setUp() {
        driver = initializeDriver();
        verifyProductsMethodsPage = new VerifyProductsMethodsPage(initializeDriver());
        logger.info("Go to landing page");
        driver.manage().window().maximize();
        driver.get(url);
    }

    @Test
    public void verifyPopularProducts() {
        verifyProductsMethodsPage.verifyProducts(locPopular, "Popular");
    }

    @Test
    public void verifyBestSellers() {
        verifyProductsMethodsPage.verifyProducts(locBestSellers, "BestSellers");
    }

}
