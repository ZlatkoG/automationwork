package tests;

import drivers.Driver;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.Screenshot;
import utils.StringUtils;

public class BaseTest {

    private static Logger logger = LogManager.getLogger(BaseTest.class);

    private static WebDriver driver;
    public String url = "http://automationpractice.com";

    @BeforeSuite
    public void beforeSuite() {
        logger.info("Creating screenshots directory");
        Screenshot.createScreenshotsDirectory();
        driver = Driver.getDriver();
        logger.info("Initializing driver");
    }

    @AfterMethod
    public void screenShotOnFailure(ITestResult result) {
        if (ITestResult.FAILURE == result.getStatus()) {
            logger.info("Test failed, taking screenshot");
            Screenshot.takeScreenShot(driver, StringUtils.timestamp());
        }
    }

    @AfterSuite
    public void tearDown() {
        logger.info("Shutting down driver");
        if(null != driver) {
            driver.quit();
        }
    }

    public static WebDriver initializeDriver() {
        if (driver == null) {
            driver = Driver.getDriver();
        }
        return driver;
    }
}
