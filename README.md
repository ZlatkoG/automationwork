Test automation project which tests functionalities of said application.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine.

### Running the tests
To run all GUI tests, navigate to root directory and run:

`mvn test -Dsuite=ui-test`

To run all headless tests, navigate to root directory and run:

`mvn test -Dsuite=ui-test -Dheadless=true`

### Built With

- [Maven](https://maven.apache.org/) - Build Tool
- [Selenium WebDriver](https://www.selenium.dev/) - UI Test Automation Tool

### Dependencies
- selenium-java
- selenium-server
- selenium-chrome-driver
- log4j-core
- log4j-api
- testng

### Author
- **Zlatko Georgievski**